// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class unreal_git_11_3 : ModuleRules
{
	public unreal_git_11_3(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}

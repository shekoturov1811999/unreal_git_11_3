// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_git_11_3GameMode.h"
#include "unreal_git_11_3HUD.h"
#include "unreal_git_11_3Character.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_git_11_3GameMode::Aunreal_git_11_3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aunreal_git_11_3HUD::StaticClass();
}

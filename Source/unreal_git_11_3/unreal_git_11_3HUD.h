// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "unreal_git_11_3HUD.generated.h"

UCLASS()
class Aunreal_git_11_3HUD : public AHUD
{
	GENERATED_BODY()

public:
	Aunreal_git_11_3HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

